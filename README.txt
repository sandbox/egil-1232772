# SUMMARY #

This Drupal module is a unobtrusive input filters which enables
syntax highlighting of source code snippets. The module uses the 
very popular google-code-prettify JavaScript library to do the 
highlighting.

For a full description visit the project page:
  http://drupal.org/sandbox/egil/1232772

Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/1232772


# REQUIREMENTS #

* Libraries API module (http://drupal.org/project/libraries).


# INSTALLATION #

1. Download and install as usual, see [d1] for more details.
2. Go to the google-code-prettify download page [gcp1] and 
   download the minified source files (e.g. prettify-small-1-Jun-2011.tar.bz2)
   and unpack the downloaded file into your sites/all/libraries 
   (or sites/sitename/libraries) folder.
   
   The downloaded compressed file should contain a folder named 
   google-code-prettify that contains the minified source files 
   e.g. prettify.js and prettify.css.

   The folder structure should look like this when you are done:
   sites/all/libraries/google-code-prettify with prettify.js
   and prettify.css inside it, along with any lang-*.js files you 
   decide to include.

3. Go to Text formats (admin/config/content/formats) and enable 
   Prettify for the text formats you want to enable it for.
   
4. (Optional) Go to the themes page [gcp2] and download a different
   stylesheet and rename it to prettify.css (to override the default
   prettify.css that comes with the library) and include that in 
   your theme.

[d1]: http://drupal.org/documentation/install/modules-themes/modules-7
[gcp1]: http://code.google.com/p/google-code-prettify/downloads/list
[gcp2]: http://google-code-prettify.googlecode.com/svn/trunk/styles/index.html


# CONTACT #

Current maintainers:
* Egil Hansen (egil) - http://drupal.org/user/54136